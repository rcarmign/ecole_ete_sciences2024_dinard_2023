# <center> Machine learning</center>
## <center> Introduction et quelques examples pour le sport </center>
### <center> École d'été <span style="font-variant:small-caps;">Sciences</span><sup>2024</sup> Édition 2023</center>


<center> Rémi Carmigniani</center>

Ce git est composé d'un dossier "presentation" et d'un dossier "Examples". La présentation a été faite à l'aide de [Quarto](https://quarto.org/docs/presentations) et est disponible à ce lien :
https://remi-carmigniani.quarto.pub/ecole_ete_sc2024_2023_machine_learning/


Le dossier "Examples" reprend les différents exemples de la présentation :

- [Image Classification](Examples/Classification/example_classification.ipynb) : 
        
    dans cet exemple, nous présentons comment utiliser un classifier en repartant du github : https://github.com/awinml/sports-image-classification
    
    Le lien est donné dans la présentation.
    
    Pour utiliser ce notebook, il faut télécharger le dataset à ce [lien](https://drive.google.com/drive/folders/18-DUykudGmWUQXhzHmBSkHs-WBBaLKwv?usp=sharing).
    
    Il faut noter que ce dataset est légérement modifié par rapport à l'original d'où le nouveau lien. On y a ajouté le handball. Le dataset original est disponible au lien du git source. 
    
    C'est un exemple de **transfer learning**.

- [Régression](Examples/Regression/Regression.ipynb) :

    Exemple simple sur une régression linéaire $y = a \times x +b$ en utilisant *sklearn*. Données disponible [ici](https://drive.google.com/file/d/1VERhSiHqhrCdAOdnYJar2s_VYAn-ocZj/view?usp=sharing)
    
    Exemple sur l'évolution de la vitesse des animaux. Données issues de l'article de [Hirt *et al.*](https://www.nature.com/articles/s41559-017-0241-4) disponible [ici](https://drive.google.com/file/d/1mOHDHsqH4KdO456KrsHBl6pNoo3Ccfkr/view?usp=sharing)
    
- [Clustering d'images](Examples/Clustering/face_clustering.ipynb) :

    Cet exemple est très largement inspiré du Livre [Introduction to Machine Learning](https://www.wolfram.com/language/introduction-machine-learning/) de Etienne Bernard que je recommande de lire. Les images sont disponibles [ici](https://drive.google.com/drive/folders/1LtshfLamo6ydtbbwHIjV6IXW2GHlETvi?usp=sharing).

- [Dimensionality Reduction](Examples/Dimensionality_reduction/Dimension_reduction.ipynb) :

    Là encore le premier exemple est adapté du livre précédent. La nouveauté est le développement d'une transformation inverse qui a ma connaissance n'était pas disponible sur *sklearn*
    
    Un second exemple est proposé avec des données de forme de corps de nageur obtenu avec notre code d'estimation de pose, développé dans le cadre de l'ANR NePTUNE (ANR-19-STHP-0004-NePTUNE). Les données sont disponibles [ici](https://drive.google.com/drive/folders/1DTzdqx4wCKoO5eYetTlDlE-Y8U6YFbUi?usp=sharing)
    
    
- [Estimation de position](Examples/Estimation_de_pose/PoseEstimation.ipynb) :

    Ici un exemple d'utilisation d'OpenPose en Python. Le model peut être téléchargé [ici](https://drive.google.com/drive/folders/1fn1egqa3Cue5jn85U4BhIKlbD6TE6O7i?usp=sharing). L'application est inspiré du git [Open-Pose-Keras](https://github.com/cchamber/Open-Pose-Keras) dont certaines fonctions sont reprises. 
    
    On présente également dans ce notebook la construction d'un UNet. On peut retrouver les deux fonctions dans différents forum.
    
    
    
    
    
    
    



