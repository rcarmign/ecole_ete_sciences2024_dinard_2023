import pickle
import tensorflow as tf
import numpy as np
import pandas as pd
import glob
import imgaug.augmenters as iaa
from numpy import ma
import random
import cv2


class CustomDataGen(tf.keras.utils.Sequence):

    def __init__(self, data_dir, batch_size, shuffle,flip,augcolor=True):
        self.data_dir = data_dir
        self.batch_size = batch_size
        self.path_col = 'path'
        self.class_col='sport'
        self.label_col='label'
        
        self.df = self.__create_dataframe()
        self.shuffle = shuffle
        self.flip=flip
        self.flip_data=0
        self.augcolor=augcolor
        self.aug = iaa.Sequential([
    iaa.WithBrightnessChannels(iaa.Add((-20, 20))),
    iaa.MultiplyHue((0.9, 1.1)),
    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.02*255), per_channel=1)])
        
    def __create_dataframe(self):
        data_dir =self.data_dir
        classes = np.sort(np.array(glob.glob(str(data_dir) + "/*")))
        for i in range(len(classes)):
            classes[i]=classes[i].replace(data_dir+'/','') 
        name =[]
        path = []
        label=[]
        for i in range(len(classes)):
                    sport_name = classes[i]
                    label_number = i
                    list_image = glob.glob(str(data_dir)+'/'+sport_name+'/*.jpg')
                    for file in list_image:
                        name.append(sport_name)
                        path.append(file)
                        label.append(i)
        data = {'path':path,'class':name,'label':label}
        data_frame = pd.DataFrame(data=data)
        return data_frame

    def on_epoch_end(self):
        if self.shuffle:
            self.df = self.df.sample(frac=1).reset_index(drop=True)

    def __get_input(self, path):
        #input is an image
        path_image = str(path)
        image = cv2.imread(path)
        if image is None:
            print(path)
        image=np.uint8(image)
        if self.augcolor:
            aug_det = self.aug.to_deterministic()
            image=aug_det(images=np.array([image]))[0]
        if self.flip:
            image=np.flip(image,axis=1)
        X_output = image[:,:,[2,1,0]] #convert to RGB as the previous network
        return X_output

    def __get_output_label(self, label):
        Y_output=label
        return Y_output
    def __get_data(self, batches):
        #flip a batch
        if self.flip :
            #print(random.uniform(0,1))
            if random.uniform(0,1)>0.5:
                self.flip_data=1
            else:
                self.flip_data=0
        # Generates data containing batch_size samples
        path_batch = batches[self.path_col]
        label_batch = batches[self.label_col]

        X_batch = np.asarray([self.__get_input(x) for x in path_batch])

        y_batch = np.asarray([self.__get_output_label(x) for x in label_batch])

        return X_batch,y_batch

    def __getitem__(self, index):
        batches = self.df[index * self.batch_size:(index + 1) * self.batch_size]
        X, y = self.__get_data(batches)
        return X, y

    def __len__(self):
        return len(self.df) // self.batch_size